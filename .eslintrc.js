module.exports = {
  plugins: ['prettier', 'eslint: recommended'],
  extends: ['prettier'],
  root: true,
  env: {
    node: true,
    jest: true,
    commonjs: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    'no-prototype-builtins': 'off',
  },
};
