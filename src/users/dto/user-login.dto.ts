export class UserLoginDTO {
  login: string;
  password: string;
}

export class UserLoggedDTO {
  tokenlogin: string;
  password: string;
}
