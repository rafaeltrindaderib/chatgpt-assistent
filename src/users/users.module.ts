import { Module } from '@nestjs/common';
import { UsersService } from '@src/users/services/users.service';
import { UsersController } from '@src/users/users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '@src/users/schemas/user.schema';

@Module({
  controllers: [UsersController],
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
