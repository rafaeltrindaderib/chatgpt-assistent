import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './services/users.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { getModelToken } from '@nestjs/mongoose';
import { User } from './schemas/user.schema';

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;

  beforeEach(async () => {
    const mockUsersService = {
      findAll: jest.fn(),
      findOne: jest.fn(),
      update: jest.fn(),
      remove: jest.fn(),
    };

    const mockUserModel = {};

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [{ provide: UsersService, useValue: mockUsersService }, {
        provide: getModelToken(User.name),
        useValue: mockUserModel,
      },],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call findAll method', async () => {
    await controller.findAll();
    expect(service.findAll).toHaveBeenCalled();
  });

  it('should call findOne method with expected param', async () => {
    const id = 'someId';
    await controller.findOne(id);
    expect(service.findOne).toHaveBeenCalledWith(id);
  });

  it('should call update method with expected params', async () => {
    const id = 'someId';
    const updateUserDto = new UpdateUserDto();
    await controller.update(id, updateUserDto);
    expect(service.update).toHaveBeenCalledWith(id, updateUserDto);
  });

  it('should call remove method with expected param', async () => {
    const id = 'someId';
    await controller.remove(id);
    expect(service.remove).toHaveBeenCalledWith(id);
  });
});
