import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AuthMiddleware } from '@src/middlewares/auth.middleware';
import { AppController } from '@src/app.controller';
import { AppService } from '@src/app.service';
import { UsersModule } from '@src/users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ApiModule } from '@src/api/api.module';
import { ConversationModule } from '@src/conversation/conversation.module';
import { LojaintegradaModule } from '@src/lojaintegrada/lojaintegrada.module';
import 'dotenv/config';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGO_URL),
    UsersModule,
    ApiModule,
    ConversationModule,
    LojaintegradaModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor() {}
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: 'lojaintegrada/webhook', method: RequestMethod.POST });
  }
}
