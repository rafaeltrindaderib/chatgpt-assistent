import { Module } from '@nestjs/common';
import { ApiService } from '@src/api/api.service';
import { ApiController } from '@src/api/api.controller';
import { HttpModule } from '@nestjs/axios';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ConversationService } from '@src/conversation/conversation.service';

@Module({
  imports: [
    HttpModule,
    MulterModule.register({
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          cb(null, `${file.originalname}`);
        },
      }),
    }),
  ],
  controllers: [ApiController],
  providers: [ApiService, ConversationService],
})
export class ApiModule {}
