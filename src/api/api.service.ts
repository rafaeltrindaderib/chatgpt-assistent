import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import OpenAI from 'openai';
import axios from 'axios';
import * as fs from 'fs';
import * as path from 'path';
import * as FormData from 'form-data';
import { ConversationService } from '@src/conversation/conversation.service';

@Injectable()
export class ApiService {
  constructor(private readonly conversationService: ConversationService) { }

  private openai = new OpenAI({ apiKey: process.env.OPENAI_API_KEY });

  async processAudio(file: Express.Multer.File): Promise<string> {
    const transcription = await this.transcribeAudio(file);
    this.conversationService.addMessage('1', {
      sender: 'user',
      content: transcription,
    });

    const response = await this.generateResponse('1');

    this.conversationService.addMessage('1', {
      sender: 'bot',
      content: response,
    });

    return await this.synthesizeSpeech(response);
  }

  private async transcribeAudio(file: Express.Multer.File): Promise<string> {
    if (!file || !file.path) {
      throw new HttpException(
        'Arquivo de áudio inválido',
        HttpStatus.BAD_REQUEST
      );
    }

    const formData = new FormData();
    formData.append('file', fs.createReadStream(file.path));
    formData.append('model', 'whisper-1');
    formData.append('response_format', 'text');

    try {
      const response = await axios.post(
        'https://api.openai.com/v1/audio/transcriptions',
        formData,
        {
          headers: {
            Authorization: `Bearer ${process.env.OPENAI_API_KEY}`,
            ...formData.getHeaders(),
          },
        }
      );
      console.log(response.data);
      return response.data;
    } catch (error) {
      throw new HttpException(
        'Erro ao transcrever o áudio',
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  private async generateResponse(conversationId: string): Promise<string> {
    // Recuperar o histórico de mensagens para a conversa
    const conversation =
      this.conversationService.getConversation(conversationId);
    const messages: any = [
      {
        role: 'system',
        content:
          'Você é meu professor de ingles, me ajude com entrevistas,me ensine palavras novas, me ajude a extender meu vocabulario, a primeira coisa que você deve fazer ao iniciar a conversa é perguntar meu nome e oque eu gostaria de fazer, e me dar opções de coisas que posso fazer para aprender com você.',
      },
    ];
    console.log(conversation);
    messages.push(
      ...(conversation?.messages ?? []).map((msg) => ({
        role: msg?.sender === 'user' ? 'user' : 'assistant',
        content: msg?.content,
      }))
    );
    console.log(messages);
    // Chamar a OpenAI com o histórico completo como contexto
    const completion = await this.openai.chat.completions.create({
      messages,
      model: 'gpt-3.5-turbo',
    });

    console.log(completion.choices[0].message.content);
    return completion.choices[0].message.content;
  }

  public async synthesizeSpeech(text: string, title?: string): Promise<string> {
    const speechFile = path.resolve(`./uploads/${title ? title : 'audio'}.wav`);

    try {
      const mp3 = await this.openai.audio.speech.create({
        model: 'tts-1-hd',
        voice: 'onyx',
        input: `
        O Futuro dos Óculos de Realidade Aumentada
        À medida que a tecnologia por trás dos óculos de realidade aumentada continua a evoluir, seu potencial para moldar nosso futuro é ilimitado. Estamos à beira de uma nova era de interação digital, onde as informações e o conteúdo digital serão integrados de forma transparente ao nosso ambiente físico, criando experiências que hoje só podemos imaginar. Com o avanço contínuo da tecnologia de RA, os próximos anos prometem trazer inovações ainda mais revolucionárias.
        
        Conclusão
        Os óculos de realidade aumentada estão abrindo portas para um mundo onde a linha entre o digital e o real é cada vez mais tênue. Eles têm o potencial de transformar radicalmente a forma como vivemos, trabalhamos e interagimos uns com os outros. Enquanto enfrentamos os desafios que essa nova tecnologia traz, também nos deparamos com a empolgante promessa de um futuro repleto de possibilidades ilimitadas. À medida que continuamos a explorar e expandir os limites da realidade aumentada, estamos não apenas moldando o futuro da tecnologia, mas também o futuro da experiência humana. Este é apenas o começo de uma jornada emocionante no vasto mundo da realidade aumentada, uma jornada que promete redefinir nossa percepção da realidade como a conhecemos.
           
     `
      });

      const buffer = Buffer.from(await mp3.arrayBuffer());
      await fs.promises.writeFile(speechFile, buffer);
      console.log(speechFile);

      return speechFile;
    } catch (error) {
      throw new HttpException(
        'Erro ao sintetizar a fala',
        HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
  }

  public async transcribe() {
    // const speechFile = path.resolve(`./uploads/anped.mp3`);
    // const simulatedMulterFile: Express.Multer.File = {
    //   fieldname: '', // Pode ser uma string vazia, não é usado na transcrição
    //   originalname: 'anped.mp3',
    //   encoding: '', // Pode ser uma string vazia, não é usado na transcrição
    //   mimetype: 'audio/mp3',
    //   size: fs.statSync(speechFile).size,
    //   destination: './uploads',
    //   filename: 'anped.mp3',
    //   path: speechFile,
    //   buffer: Buffer.alloc(0), // Não necessário para transcrição, pois o método usa 'path'
    //   stream: fs.createReadStream(speechFile) // Não é diretamente necessário, mas incluído para completude
    // };
    return await this.synthesizeSpeech('','video3')
  }
}
