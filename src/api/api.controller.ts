import {
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
  Res,
  HttpException,
  HttpStatus,
  Body,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiService } from './api.service';
import { Response } from 'express';
import { Express } from 'express';
import * as path from 'path';

@Controller('api')
export class ApiController {
  constructor(private readonly apiService: ApiService) { }
  @Post('upload')
  @UseInterceptors(FileInterceptor('audio'))
  async uploadAudio(
    @UploadedFile() file: Express.Multer.File,
    @Res() res: Response
  ) {
    console.log(file);
    if (!file) {
      throw new HttpException('Arquivo não fornecido', HttpStatus.BAD_REQUEST);
    }
    try {
      const audioResponse = await this.apiService.processAudio(file);
      console.log(audioResponse);
      res.sendFile(path.resolve(audioResponse));
    } catch (error) {
      console.log(error);
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('audio')
  async createAnAudio(
    @Res() res: Response,
    @Body() createAudioDto: {
      text: string,
      title: string
    }
  ) {
    try {
      await this.apiService.synthesizeSpeech(createAudioDto.text, createAudioDto.title)
      return res.send({ message: 'Audio criado com sucesso' })
    }
    catch (e) {
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('transcribe')
  async transcribeVideo(){
    return await this.apiService.transcribe()
  }
}
