import { PartialType } from '@nestjs/mapped-types';
import { CreateLojaintegradaDto } from './create-lojaintegrada.dto';

export class UpdateLojaintegradaDto extends PartialType(CreateLojaintegradaDto) {}
