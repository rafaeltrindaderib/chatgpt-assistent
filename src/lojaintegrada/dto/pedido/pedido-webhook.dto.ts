export type PedidoWebhookDto = {
  token: string;
  tipo: string;
  id: number;
  id_externo: number | null;
  numero: number;
  valor_desconto: number;
  valor_envio: number;
  valor_subtotal: number;
  valor_total: number;
  utm_campaign: string | null;
  peso_real: number;
  cliente_obs: string | null;
  id_anymarket: number | null;
  data_criacao: string;
  data_modificacao: string;
  data_expiracao: string;
  cupom_desconto: {
    id: number | null;
    codigo: string | null;
    tipo: string | null;
  };
  cliente: ClienteWebhookDto;
  endereco_entrega: {
    id: number;
    tipo: string;
    ie: string | null;
    cnpj: string | null;
    cpf: string;
    rg: string | null;
    nome: string;
    razao_social: string | null;
    endereco: string;
    numero: string;
    complemento: string;
    referencia: string;
    bairro: string;
    cidade: string;
    estado: string;
    cep: string;
    pais: string;
  };
  endereco_pagamento: {
    id: number;
    tipo: string | null;
    ie: string | null;
    cnpj: string | null;
    rg: string | null;
    nome: string;
    razao_social: string | null;
    endereco: string;
    numero: string;
    complemento: string;
    referencia: string;
    bairro: string;
    cidade: string;
    estado: string;
    cep: string;
    pais: string;
  };
  envios: {
    id: number;
    objeto: string | null;
    prazo: number;
    valor: number | null;
    data_criacao: string;
    data_modificacao: string;
    forma_envio: {
      id: number;
      codigo: string;
      nome: string;
    };
  }[];

  situacao: {
    id: number;
    codigo: string;
    nome: string;
    aprovado: boolean;
    cancelado: boolean;
    final: boolean;
    notificar_comprador: boolean;
    padrao: boolean;
    situacao_alterada: boolean;
  };

  itens: ItensWebhookDto[];
  pagamentos: PagamentosWebhookDto[];
};

export type FormaPagamentoWebhookDto = {
  id: number;
  codigo: string;
  nome: string;
};

export type PagamentosWebhookDto = {
  id: number;
  identificador_id: number;
  authorization_code: string;
  bandeira: string;
  mensagem_gateway: string;
  codigo_retorno_gateway: string;
  transacao_id: number;
  valor: number;
  valor_pago: number;
  data_criacao: string;
  data_modificacao: string;
  forma_pagamento: FormaPagamentoWebhookDto;
};

export type ClienteWebhookDto = {
  id: number;
  email: string;
  nome: string;
  telefone_celular: string;
  telefone_comercial: string | null;
  telefone_principal: string;
  situacao: string;
  data_nascimento: string;
  sexo: string;
  data_criacao: string;
  data_modificacao: string;
};

export type ItensWebhookDto = {
  linha: number;
  id: number;
  produto_id: number;
  produto_id_pai: number;
  sku: string;
  nome: string;
  tipo: string;
  quantidade: number;
  preco_cheio: number;
  preco_custo: number | null;
  preco_venda: number;
  preco_subtotal: number;
};
