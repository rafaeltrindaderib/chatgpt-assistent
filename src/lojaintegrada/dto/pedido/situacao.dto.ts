import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class SituacaoDTO {
  @IsBoolean()
  aprovado: boolean;

  @IsBoolean()
  cancelado: boolean;

  @IsString()
  codigo: string;

  @IsBoolean()
  final: boolean;

  @IsNumber()
  id: number;

  @IsString()
  nome: string;

  @IsBoolean()
  notificar_comprador: boolean;

  @IsBoolean()
  padrao: boolean;

  @IsString()
  resource_uri: string;
}
