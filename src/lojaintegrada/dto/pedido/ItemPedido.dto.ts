export type ItemPedidoDto = {
  cliente: string;
  data_criacao: string;
  data_expiracao: string;
  data_modificacao: string;
  id_anymarket: number | null;
  id_externo: number | null;
  numero: number;
  peso_real: number;
  resource_uri: string;
  situacao: ItemSituacaoDTO;
  utm_campaign: string | null;
  valor_desconto: number;
  valor_envio: number;
  valor_subtotal: number;
  valor_total: number;
};

export type ItemSituacaoDTO = {
  aprovado: boolean;
  cancelado: boolean;
  codigo: string;
  final: boolean;
  id: number;
  nome: string;
  notificar_comprador: boolean;
  padrao: boolean;
  resource_uri: string;
};

export type PaginationSituacaoPedidoDTO = {
  meta: {
    limit: 20;
    next: null;
    offset: 0;
    previous: null;
    total_count: 13;
  };
  objects: ItemSituacaoDTO[];
};
