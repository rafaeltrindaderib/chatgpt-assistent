import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document, ObjectId } from 'mongoose';
import { ItemModel } from './item.entity';

class EnderecoEntregaSchema {
  @Prop()
  id: number;

  @Prop()
  tipo: string;

  @Prop()
  ie: string;

  @Prop()
  cnpj: string;

  @Prop()
  cpf: string;

  @Prop()
  rg: string;

  @Prop()
  nome: string;

  @Prop()
  razao_social: string;

  @Prop()
  endereco: string;

  @Prop()
  numero: string;

  @Prop()
  complemento: string;

  @Prop()
  referencia: string;

  @Prop()
  bairro: string;

  @Prop()
  cidade: string;

  @Prop()
  estado: string;

  @Prop()
  cep: string;

  @Prop()
  pais: string;
}

class EnderecoPagamentoSchema {
  @Prop()
  id: number;

  @Prop()
  tipo: string;

  @Prop()
  ie: string;

  @Prop()
  cnpj: string;

  @Prop()
  rg: string;

  @Prop()
  nome: string;

  @Prop()
  razao_social: string;

  @Prop()
  endereco: string;

  @Prop()
  numero: string;

  @Prop()
  complemento: string;

  @Prop()
  referencia: string;

  @Prop()
  bairro: string;

  @Prop()
  cidade: string;

  @Prop()
  estado: string;

  @Prop()
  cep: string;

  @Prop()
  pais: string;
}

class FormaEnvioSchema {
  @Prop()
  id: number;

  @Prop()
  codigo: string;

  @Prop()
  nome: string;
}

class EnviosSchema {
  @Prop()
  id: number;

  @Prop()
  objeto: string;

  @Prop()
  prazo: number;

  @Prop()
  valor: number;

  @Prop()
  data_criacao: string;

  @Prop()
  data_modificacao: string;

  @Prop({ type: FormaEnvioSchema })
  forma_envio: FormaEnvioSchema;
}

class CupomDescontoSchema {
  @Prop()
  id: number;

  @Prop()
  codigo: string;

  @Prop()
  tipo: string;
}

class SituacaoSchema {
  @Prop()
  id: number;

  @Prop()
  codigo: string;

  @Prop()
  nome: string;

  @Prop()
  aprovado: boolean;

  @Prop()
  cancelado: boolean;

  @Prop()
  final: boolean;

  @Prop()
  notificar_comprador: boolean;

  @Prop()
  padrao: boolean;

  @Prop()
  situacao_alterada: boolean;
}

class FormaPagamentoSchema {
  @Prop()
  id: number;

  @Prop()
  codigo: string;

  @Prop()
  nome: string;
}

class PagamentosSchema {
  @Prop()
  id: number;

  @Prop()
  identificador_id: number;

  @Prop()
  authorization_code: string;

  @Prop()
  bandeira: string;

  @Prop()
  mensagem_gateway: string;

  @Prop()
  codigo_retorno_gateway: string;

  @Prop()
  transacao_id: number;

  @Prop()
  valor: number;

  @Prop()
  valor_pago: number;

  @Prop()
  data_criacao: string;

  @Prop()
  data_modificacao: string;

  @Prop({ type: FormaPagamentoSchema })
  forma_pagamento: FormaPagamentoSchema;
}

@Schema()
export class PedidoWebhookModel {
  @Prop()
  token: string;

  @Prop()
  tipo: string;

  @Prop()
  id: number;

  @Prop()
  id_externo: number;

  @Prop()
  numero: number;

  @Prop()
  valor_desconto: number;

  @Prop()
  valor_envio: number;

  @Prop()
  valor_subtotal: number;

  @Prop()
  valor_total: number;

  @Prop()
  utm_campaign: string;

  @Prop()
  peso_real: number;

  @Prop()
  cliente_obs: string;

  @Prop()
  id_anymarket: number;

  @Prop()
  data_criacao: string;

  @Prop()
  data_modificacao: string;

  @Prop()
  data_expiracao: string;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'ClienteModel' })
  cliente: string;

  @Prop({ type: CupomDescontoSchema })
  cupom_desconto: CupomDescontoSchema;

  @Prop({ type: EnderecoEntregaSchema })
  endereco_entrega: EnderecoEntregaSchema;

  @Prop({ type: EnderecoPagamentoSchema })
  endereco_pagamento: EnderecoPagamentoSchema;

  @Prop({ type: EnviosSchema })
  envios: EnviosSchema[];

  @Prop({ type: PagamentosSchema })
  pagamentos: PagamentosSchema[];

  @Prop({ type: SituacaoSchema })
  situacao: SituacaoSchema;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: ItemModel.name })
  itens: string[];
}

export type PedidoDocument = PedidoWebhookModel & Document;
export const PedidoWebhookSchema =
  SchemaFactory.createForClass(PedidoWebhookModel);
