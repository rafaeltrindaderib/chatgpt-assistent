// pedido.model.ts

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { ClienteModel } from './cliente.entity';

@Schema()
export class Situacao {
  @Prop()
  aprovado: boolean;

  @Prop()
  cancelado: boolean;

  @Prop()
  codigo: string;

  @Prop()
  final: boolean;

  @Prop()
  id: number;

  @Prop()
  nome: string;

  @Prop()
  notificar_comprador: boolean;

  @Prop()
  padrao: boolean;

  @Prop()
  resource_uri: string;
}

export type SituacaoDocument = Situacao & Document;

@Schema()
export class PedidoModel {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: ClienteModel.name })
  cliente: string;

  @Prop()
  data_criacao: string;

  @Prop()
  data_expiracao: string;

  @Prop()
  data_modificacao: string;

  @Prop()
  id_anymarket: number | null;

  @Prop()
  id_externo: number | null;

  @Prop()
  numero: number;

  @Prop()
  peso_real: number;

  @Prop()
  resource_uri: string;

  @Prop({ type: Situacao })
  situacao: Situacao;

  @Prop()
  utm_campaign: string | null;

  @Prop()
  valor_desconto: number;

  @Prop()
  valor_envio: number;

  @Prop()
  valor_subtotal: number;

  @Prop()
  valor_total: number;
  static schema: any;
}

export type PedidoDocument = PedidoModel & Document;

export const SituacaoSchema = SchemaFactory.createForClass(Situacao);
export const PedidoSchema = SchemaFactory.createForClass(PedidoModel);
