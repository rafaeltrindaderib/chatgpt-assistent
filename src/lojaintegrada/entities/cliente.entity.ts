import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

@Schema()
export class ClienteModel {
  @Prop({ type: String, auto: true })
  _id: string;

  @Prop()
  email: string;

  @Prop({ type: Number, unique: true }) // Se você precisa garantir unicidade para clienteId
  id: number;

  @Prop()
  nome: string;

  @Prop()
  telefone_celular: string;

  @Prop()
  telefone_comercial: string | null;

  @Prop()
  telefone_principal: string;

  @Prop()
  situacao: string;

  @Prop()
  data_nascimento: Date;

  @Prop()
  sexo: string;

  @Prop()
  data_criacao: Date;

  @Prop()
  data_modificacao: Date;
  static schema: any;
}

export type ClienteDocument = ClienteModel & Document;

export const ClienteSchema = SchemaFactory.createForClass(ClienteModel);
