import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class ProdutoModel {
  @Prop()
  linha: number;

  @Prop({ type: Number, unique: true }) // Se você precisa garantir unicidade para produtoId
  id: number;

  @Prop()
  produtoId: number;

  @Prop()
  produtoIdPai: number;

  @Prop()
  sku: string;

  @Prop()
  nome: string;

  @Prop()
  tipo: string;

  @Prop()
  quantidade: number;

  @Prop()
  preco_cheio: number;

  @Prop()
  preco_custo: number | null;

  @Prop()
  preco_venda: number;

  @Prop()
  preco_subtotal: number;
}

export type ProdutoDocument = ProdutoModel & Document;

export const ProdutoSchema = SchemaFactory.createForClass(ProdutoModel);
