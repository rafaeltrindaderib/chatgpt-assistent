import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class ItemModel {
  @Prop()
  linha: number;

  @Prop({ type: Number, unique: true }) // Se você precisa garantir unicidade para itemId
  id: number;

  @Prop()
  produto_id: number;

  @Prop()
  produto_id_pai: number;

  @Prop()
  sku: string;

  @Prop()
  nome: string;

  @Prop()
  tipo: string;

  @Prop()
  quantidade: number;

  @Prop()
  preco_cheio: number;

  @Prop()
  preco_custo: number | null;

  @Prop()
  preco_venda: number;

  @Prop()
  preco_subtotal: number;
  static schema: any;
}

export type ItemDocument = ItemModel & Document;
export const ItemSchema = SchemaFactory.createForClass(ItemModel);
