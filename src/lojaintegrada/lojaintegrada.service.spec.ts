import { Test, TestingModule } from '@nestjs/testing';
import { LojaintegradaService } from './lojaintegrada.service';

describe('LojaintegradaService', () => {
  let service: LojaintegradaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LojaintegradaService],
    }).compile();

    service = module.get<LojaintegradaService>(LojaintegradaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
