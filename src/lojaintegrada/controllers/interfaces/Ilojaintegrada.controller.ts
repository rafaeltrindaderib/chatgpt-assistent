import { CreateLojaintegradaDto } from '@src/lojaintegrada/dto/create-lojaintegrada.dto';
import { PaginationSituacaoPedidoDTO } from '@src/lojaintegrada/dto/pedido/ItemPedido.dto';
import { SituacaoDTO } from '@src/lojaintegrada/dto/pedido/situacao.dto';
import { UpdateLojaintegradaDto } from '@src/lojaintegrada/dto/update-lojaintegrada.dto';
import { ObjectId } from 'mongoose';

export interface ILojaintegradaController {
  create(createLojaintegradaDto: CreateLojaintegradaDto): any;
  findAll(): any;
  findOne(id: string): any;
  update(id: ObjectId, situacao: SituacaoDTO): any;
  remove(id: string): any;
  getPedidos(): any;
  handleWebhookEvent(event: any): any;
  getSituacaoPedido(): PaginationSituacaoPedidoDTO;
}
