import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  UseGuards,
} from '@nestjs/common';
import { LojaintegradaService } from './lojaintegrada.service';
import { CreateLojaintegradaDto } from './dto/create-lojaintegrada.dto';
import { UpdateLojaintegradaDto } from './dto/update-lojaintegrada.dto';
import { PedidoService } from './services/pedido.service';
import { ILojaintegradaController } from './controllers/interfaces/Ilojaintegrada.controller';
import { SituacaoDTO } from './dto/pedido/situacao.dto';
import { PaginationSituacaoPedidoDTO } from './dto/pedido/ItemPedido.dto';
import { Situacao } from './entities/pedido.entity';
import { ObjectId } from 'mongoose';
import { AuthGuard } from '@nestjs/passport';

@Controller('lojaintegrada')
export class LojaintegradaController implements ILojaintegradaController {
  constructor(
    private readonly lojaintegradaService: LojaintegradaService,
    private readonly pedidoService: PedidoService
  ) {}

  @Post()
  create(@Body() createLojaintegradaDto: CreateLojaintegradaDto) {
    return this.lojaintegradaService.create(createLojaintegradaDto);
  }

  @Get()
  findAll() {
    return this.lojaintegradaService.findAll();
  }

  @Get('item/:id')
  findOne(@Param('id') id: string) {
    return this.lojaintegradaService.findOne(+id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('pedido/:id')
  update(@Param('id') _id: ObjectId, @Body() situacao: Situacao) {
    return this.pedidoService.changeStatus(_id, situacao);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.lojaintegradaService.remove(+id);
  }

  @Get('pedidos')
  getPedidos() {
    return this.pedidoService.get();
  }

  @Post('webhook')
  handleWebhookEvent(@Body() event: any) {
    return this.pedidoService.handleWebhookPedidos(event);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('pedido/situacao')
  getSituacaoPedido(): PaginationSituacaoPedidoDTO {
    return this.pedidoService.getSituacoesPedido();
  }
}
