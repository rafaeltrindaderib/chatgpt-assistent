import { Module } from '@nestjs/common';
import { LojaintegradaService } from './lojaintegrada.service';
import { LojaintegradaController } from './lojaintegrada.controller';
//import { PedidoController } from '@src/lojaintegrada/pedido.controller';
import { PedidoService } from './services/pedido.service';
import { ClienteModel, ClienteSchema } from './entities/cliente.entity';
import { ItemModel, ItemSchema } from './entities/item.entity';
import { MongooseModule } from '@nestjs/mongoose';
import {
  PedidoWebhookModel,
  PedidoWebhookSchema,
} from './entities/pedido-webhook.entity';

@Module({
  controllers: [LojaintegradaController],
  providers: [LojaintegradaService, PedidoService],
  imports: [
    MongooseModule.forRoot(process.env.MONGO_URL),
    MongooseModule.forFeature([
      { name: PedidoWebhookModel.name, schema: PedidoWebhookSchema },
      { name: ClienteModel.name, schema: ClienteSchema },
      { name: ItemModel.name, schema: ItemSchema },
    ]),
  ],
})
export class LojaintegradaModule {}
