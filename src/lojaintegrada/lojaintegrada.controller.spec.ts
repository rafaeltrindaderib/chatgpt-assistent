import { Test, TestingModule } from '@nestjs/testing';
import { LojaintegradaController } from './lojaintegrada.controller';
import { LojaintegradaService } from './lojaintegrada.service';

describe('LojaintegradaController', () => {
  let controller: LojaintegradaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LojaintegradaController],
      providers: [LojaintegradaService],
    }).compile();

    controller = module.get<LojaintegradaController>(LojaintegradaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
