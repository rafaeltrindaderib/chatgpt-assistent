import { BadRequestException, Injectable } from '@nestjs/common';
import { responsePedido } from '@src/test/lojaintegrada/mocks/pedido.mock';
import { InjectModel } from '@nestjs/mongoose';
import { Model, ObjectId, Types } from 'mongoose';
import { ItemModel } from '@src/lojaintegrada/entities/item.entity';
import { ClienteModel } from '@src/lojaintegrada/entities/cliente.entity';
import {
  ItemPedidoDto,
  PaginationSituacaoPedidoDTO,
} from '@src/lojaintegrada/dto/pedido/ItemPedido.dto';
import {
  ClienteWebhookDto,
  ItensWebhookDto,
  PedidoWebhookDto,
} from '@src/lojaintegrada/dto/pedido/pedido-webhook.dto';
import { PedidoWebhookModel } from '../entities/pedido-webhook.entity';
import { responseSituacaoPedidoMock } from '@src/test/lojaintegrada/mocks/situacao-pedido.mock';
import { SituacaoDTO } from '../dto/pedido/situacao.dto';

@Injectable()
export class PedidoService {
  constructor(
    @InjectModel(PedidoWebhookModel.name)
    private pedidoModel: Model<PedidoWebhookModel>,
    @InjectModel(ClienteModel.name) private clienteModel: Model<ClienteModel>,
    @InjectModel(ItemModel.name) private itemModel: Model<ItemModel>
  ) {}
  get(): ItemPedidoDto[] {
    return responsePedido;
  }

  async handleWebhookPedidos(event: PedidoWebhookDto): Promise<string> {
    if (event.situacao.situacao_alterada) {
      switch (event.tipo) {
        case 'pedido_venda':
          const cliente = await this.createCliente(event.cliente);
          const itens = await this.createItens(event.itens);
          const pedido = await this.createPedido(event, cliente, itens);

          //  pedidoModel: PedidoWebhookDTO = this.setPedidoToDB();

          //cadastra o pedido novo
          return 'OK';

          //dispara mensagem para usuário.
          break;
      }
    }

    return '';
  }

  async create(pedidoWebhookDTO: PedidoWebhookDto) {
    const { id } = pedidoWebhookDTO;
    const hasPedido = this.pedidoModel.findOne({ id: id }).exec();
    //create cliente
    const cliente = this.pedidoModel.findOne({ id: id }).exec();

    //create itens
    if (hasPedido) {
      throw new BadRequestException('Pedido already registred');
    }
    const createdPedido = new this.pedidoModel(pedidoWebhookDTO);
    return createdPedido.save();
  }

  async createCliente(clienteDTO: ClienteWebhookDto): Promise<ClienteModel> {
    const hasCliente = await this.clienteModel
      .findOne({ id: clienteDTO.id })
      .exec();
    let createdCliente: ClienteModel;
    if (!hasCliente) {
      const createdCliente = new this.clienteModel(clienteDTO);
      return createdCliente.save();
    }

    return hasCliente;
  }

  async createItens(listIitemDTO: ItensWebhookDto[]): Promise<any> {
    const itens = listIitemDTO.map(async (item) => {
      const hasItem = await this.itemModel.findOne({ id: item.id }).exec();
      if (!hasItem) {
        const createdItem = new this.itemModel(item);
        await createdItem.save();
        return createdItem._id;
      }
      return hasItem._id;
    });

    const ids = await Promise.all(itens);

    return ids;
  }

  async createPedido(
    pedidoDTO: PedidoWebhookDto,
    cliente: ClienteModel,
    itens_id: string[]
  ): Promise<any> {
    const hasItem = await this.pedidoModel.findOne({ id: pedidoDTO.id }).exec();
    if (!hasItem) {
      const item: PedidoWebhookModel = {
        token: pedidoDTO.token,
        tipo: pedidoDTO.tipo,
        id: pedidoDTO.id,
        id_externo: pedidoDTO.id_externo,
        numero: pedidoDTO.numero,
        valor_desconto: pedidoDTO.valor_desconto,
        valor_envio: pedidoDTO.valor_envio,
        valor_subtotal: pedidoDTO.valor_subtotal,
        valor_total: pedidoDTO.valor_total,
        utm_campaign: pedidoDTO.utm_campaign,
        peso_real: pedidoDTO.peso_real,
        cliente_obs: pedidoDTO.cliente_obs,
        id_anymarket: pedidoDTO.id_anymarket,
        data_criacao: pedidoDTO.data_criacao,
        data_modificacao: pedidoDTO.data_modificacao,
        data_expiracao: pedidoDTO.data_expiracao,
        cliente: cliente._id,
        cupom_desconto: pedidoDTO.cupom_desconto,
        endereco_entrega: pedidoDTO.endereco_entrega,
        endereco_pagamento: pedidoDTO.endereco_pagamento,
        envios: pedidoDTO.envios,
        itens: itens_id,
        pagamentos: pedidoDTO.pagamentos,
        situacao: pedidoDTO.situacao,
      };

      const createdItem = new this.pedidoModel(item);
      await createdItem.save();
      return createdItem._id;
    }
  }

  async changeStatus(id: ObjectId, situacao: SituacaoDTO): Promise<any> {
    let itemPedido;
    try {
      itemPedido = await this.pedidoModel.findOne({ _id: id }).exec();
      if (itemPedido) {
        itemPedido.situacao.situacao_alterada = true;
        itemPedido.situacao.aprovado = situacao.aprovado;
        itemPedido.situacao.cancelado = situacao.cancelado;
        itemPedido.situacao.codigo = situacao.codigo;
        itemPedido.situacao.final = situacao.final;
        itemPedido.situacao.id = situacao.id;
        itemPedido.situacao.nome = situacao.nome;
        itemPedido.situacao.notificar_comprador = situacao.notificar_comprador;
        itemPedido.situacao.padrao = situacao.padrao;
        itemPedido.save();
        return itemPedido;
      }
    } catch (error) {
      throw new Error('Erro ao atualizar status do pedido');
    }

    return itemPedido;
  }

  getSituacoesPedido(): PaginationSituacaoPedidoDTO {
    return responseSituacaoPedidoMock;
  }
}
