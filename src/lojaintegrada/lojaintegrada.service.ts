import { Injectable } from '@nestjs/common';
import { CreateLojaintegradaDto } from './dto/create-lojaintegrada.dto';
import { UpdateLojaintegradaDto } from './dto/update-lojaintegrada.dto';
import { SituacaoDTO } from './dto/pedido/situacao.dto';

@Injectable()
export class LojaintegradaService {
  create(createLojaintegradaDto: CreateLojaintegradaDto) {
    return 'This action adds a new lojaintegrada';
  }

  findAll() {
    return `This action returns all lojaintegrada`;
  }

  findOne(id: number) {
    return `This action returns a #${id} lojaintegrada`;
  }

  update(id: number, situacao: SituacaoDTO) {
    return `This action updates a #${id} lojaintegrada`;
  }

  remove(id: number) {
    return `This action removes a #${id} lojaintegrada`;
  }
}
