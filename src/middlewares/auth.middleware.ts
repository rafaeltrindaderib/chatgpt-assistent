import { HttpException, HttpStatus, Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';


@Injectable()
export class AuthMiddleware implements NestMiddleware {
  private readonly authToken: string = process.env.OUR_API_KEY || 'default-token';

  async use(req: any, res: Response, next: NextFunction) {
    try{
      const authHeaders = req.headers.authorization ;
      if (authHeaders && (authHeaders as string).split(' ')[1]) {
        const token = (authHeaders as string).split(' ')[1];
        jwt.verify(token, this.authToken);
        next();
  
      } else {
        throw new HttpException('Not authorized.', HttpStatus.UNAUTHORIZED);
      }
    }
    catch (e) {
      throw new HttpException('Token expirado ou invalido.', HttpStatus.UNAUTHORIZED);
    }
  }
}
