import { Module } from '@nestjs/common';
import { ConversationService } from '@src/conversation/conversation.service';
import { ConversationController } from '@src/conversation/conversation.controller';

@Module({
  controllers: [ConversationController],
  providers: [ConversationService],
})
export class ConversationModule {}
