import { Controller, Post, Get, Body, Param } from '@nestjs/common';
import { ConversationService } from '@src/conversation/conversation.service';
import { CreateMessageDto } from '@src/conversation/dto/create-message.dto';

@Controller('conversations')
export class ConversationController {
  constructor(private readonly conversationService: ConversationService) {}

  @Post(':id/messages')
  addMessage(
    @Param('id') id: string,
    @Body() createMessageDto: CreateMessageDto
  ) {
    this.conversationService.addMessage(id, createMessageDto);
  }

  @Get(':id')
  getConversation(@Param('id') id: string) {
    return this.conversationService.getConversation(id);
  }
}
