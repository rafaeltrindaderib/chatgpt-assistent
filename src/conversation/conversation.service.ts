// conversation.service.ts
import { Injectable } from '@nestjs/common';
import { Message } from '@src/conversation/interface/message.interface';
import { CreateMessageDto } from '@src/conversation/dto/create-message.dto';

@Injectable()
export class ConversationService {
  private conversations: Record<string, { id: string; messages: Message[] }> =
    {};

  addMessage(conversationId: string, createMessageDto: CreateMessageDto): void {
    const newMessage: Message = {
      ...createMessageDto,
      timestamp: new Date(),
    };

    if (!this.conversations[conversationId]) {
      this.conversations[conversationId] = { id: conversationId, messages: [] };
    }
    this.conversations[conversationId].messages.push(newMessage);
  }

  getConversation(conversationId: string): { id: string; messages: Message[] } {
    console.log(this.conversations);

    return this.conversations[conversationId];
  }
}
