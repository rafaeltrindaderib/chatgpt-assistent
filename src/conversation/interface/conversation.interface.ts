import { Message } from '@src/conversation/interface/message.interface';

export interface Conversation {
  id: string;
  messages: Message[];
}
