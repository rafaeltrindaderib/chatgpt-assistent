import { ItemPedidoDto } from '@src/lojaintegrada/dto/pedido/ItemPedido.dto';

export const responsePedido: ItemPedidoDto[] = [
  {
    cliente: '/api/v1/cliente/34220641',
    data_criacao: '2022-10-31T12:17:51.633657',
    data_expiracao: '2022-11-06T12:17:51.733114',
    data_modificacao: '2022-10-31T12:17:58.949670',
    id_anymarket: null,
    id_externo: null,
    numero: 164,
    peso_real: 0.17,
    resource_uri: '/api/v1/pedido/164',
    situacao: {
      aprovado: false,
      cancelado: true,
      codigo: 'pedido_cancelado',
      final: true,
      id: 8,
      nome: 'Pedido Cancelado',
      notificar_comprador: true,
      padrao: false,
      resource_uri: '/api/v1/situacao/8',
    },
    utm_campaign: null,
    valor_desconto: 0.0,
    valor_envio: 21.38,
    valor_subtotal: 85.0,
    valor_total: 106.38,
  },
  {
    cliente: '/api/v1/cliente/34220641',
    data_criacao: '2022-10-31T12:28:05.704751',
    data_expiracao: '2022-11-06T12:28:05.782308',
    data_modificacao: '2022-10-31T12:28:12.653648',
    id_anymarket: null,
    id_externo: null,
    numero: 165,
    peso_real: 0.45,
    resource_uri: '/api/v1/pedido/165',
    situacao: {
      aprovado: false,
      cancelado: true,
      codigo: 'pedido_cancelado',
      final: true,
      id: 8,
      nome: 'Pedido Cancelado',
      notificar_comprador: true,
      padrao: false,
      resource_uri: '/api/v1/situacao/8',
    },
    utm_campaign: null,
    valor_desconto: 0.0,
    valor_envio: 21.38,
    valor_subtotal: 11.1,
    valor_total: 32.48,
  },
];
